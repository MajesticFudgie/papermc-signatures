# PaperMC Unofficial Signature Generation Server
That's a mouthful of words!

## About
This is a pretty simple HTTP server that generates signature images on the fly.
Currently images are not cached and are generated on demand.
As a result it is NOT recommended to hotlink to this server and instead
you download the generate image and self host it.

In future images will be cached so hotlinking will be possible and avatars will automatically update.

## Requirements
The server is as light as I can make it, it depends on the following external dependencies:

 - Redis Server

## Running the server
Running the signature server is very simple to do.
```bash
	npm install
	node .
```

You may wish to create a startup script, like so:
```bash
	screen -dmS papersig node .
```

## Forks, PRs and whatnot
I certainly do appreciate pull requests to help fix issues.
However, please do not fork this project to create your own without opening an issue and asking me first.

If you really realy want to fork this, go ahead. I can't stop you.
But please do substitute the PaperMC logo and the generated fonts for your own.