/*
 * PaperMC Unofficial Signature Generation Server
 * 	Written by Thomas Edwards (MajesticFudgie)
 *
 * All contents of this repository unless otherwise specified is property of
 * Thomas Edwards and may only be taken out of its full form with permission.
 *
 * The following content belongs to its copyright holders:
 * Open Sans Regular Font
 * Razer84 Font
 * PaperMC graphics and names.
 *
 * Copyright (c) Thomas Edwards 2017
*/
const Jimp = require('jimp'),
	uuidV4 = require('uuid/v4'),
	Hapi = require('hapi'),
	Inert = require('inert'),
	Joi = require('joi'),
	manifest = require('./package.json'),
	crypto = require('crypto'),
	request = require('request-promise'),
	Boom = require('boom'),
	redis = require("redis"),
	fs = require('fs');

// Default Configuration
let config = {
	cache: true,
	redis: {
		host: "127.0.0.1",
		port: 6379,
	},
	hapi: {
		address: "0.0.0.0",
		port: 8512,
	},
};

// Setup Redis & Hapi
let redisClient, hapi;

// Get started!
loadConfig()
.then( () => {
	console.log("Loaded configuration");
	if (!config.cache) {
		console.log("WARNING: Caching is disabled. This may cause extra load on the server. Please disable caching in production!");
	}
	let assets = {
		fonts: [
			{ id: "username", source: "assets/fonts/username.fnt" },
			{ id: "open_sans", source: "assets/fonts/open_sans.fnt" },
		],
		images: [
			{ id: "background", source: "assets/images/background.png" },
			{ id: "background_gradient", source: "assets/images/background_gradient.png" },
			{ id: "paper_logo", source: "assets/images/paper_logo.png" },
		],
	}
	
	hapi = new Hapi.Server();
	redisClient = redis.createClient(config.redis);
	
	hapi.register(Inert, () => {});
	hapi.connection(config.hapi);
	hapi.start( err => {
		console.log("Hapi Started");
	});

	hapi.route({
		method: 'GET',
		path: '/',
		handler: (request, reply) => {
			reply({
				name: "PaperMC Signature Generation Server",
				homepage: manifest.homepage,
				version: manifest.version
			});
		},
	});

	hapi.route({
		method: 'GET',
		path: '/gui',
		handler: (request, reply) => {
			reply.file("index.html");
		},
	});
	
	hapi.route({
		method: 'GET',
		path: '/assets/{param*}',
		handler: {
			directory: {
				path: 'assets',
				redirectToSlash: true,
				index: true
			}
		}
	});
	
	// To pass it to the next step (admittedly hacky)
	return assets;
})
.then(loadAssets)
.then( assets => {
	
	hapi.route({
		method: 'GET',
		path: '/generate',
		
		config: {
			validate: {
				query: {
					gradient: Joi.boolean().optional().default(false),
					user: Joi.string().max(32).required(),
					color: Joi.string().hex().optional().default("0059ffff"),
					barcolor: Joi.string().hex().optional().default("0059ffff"),
					tagcolor: Joi.string().hex().optional().default("444444ff"),
					tagline: Joi.string().optional().default("I'm using PaperMC"),
					bar: Joi.number().optional().min(0).max(10).default(0),
					background: Joi.string().uri().optional().default(null),
				}
			}
		},
		
		handler: (request, reply) => {
			console.log(`Incoming Signature Request from ${request.info.remoteAddress}`);
			if (request.headers.referer) {
				console.log(`Referral: ${request.headers.referer}`);
			}
			console.log("Generating image...");
			
			if (request.query.color.length == 6) {
				request.query.color = `${request.query.color}FF`;
			}
			
			if (request.query.tagcolor.length == 6) {
				request.query.tagcolor = `${request.query.tagcolor}FF`;
			}
			
			if (request.query.barcolor.length == 6) {
				request.query.barcolor = `${request.query.barcolor}FF`;
			}
			
			getUser(request.query.user).then( user => {
				//if (!user) reply(Boom.badRequest());
				const opts = {
					bar: request.query.bar,
					size: {
						width: 500,
						height: 100,
					},
					logo : false,
					tagline: request.query.tagline,
					gradient: request.query.gradient,
					background: request.query.background,
					colour: {
						accent: Jimp.intToRGBA(parseInt(request.query.color, 16)),
						tag: Jimp.intToRGBA(parseInt(request.query.tagcolor, 16)),
						bar: Jimp.intToRGBA(parseInt(request.query.barcolor, 16)),
					}
				}
				if (user != null) {
					opts.profile = user;
					
					if (request.query.user.toLowerCase() == "paper-mc") {
						opts.logo = true;
					}
				} else {
					opts.logo = true;
					opts.tagline = "No such user!";
					opts.profile = {name: "Whoops!"};
				}
				
				let digest = crypto.createHmac('sha1', "PaperMCRules").update(JSON.stringify(opts).toLowerCase()).digest('hex');
				
				redisClient.get(`paper:signature:${digest}`, (err, val) => {
					if (val != null && config.cache) {
						console.log("Responding with cached image");
						let img = Buffer.from(val, 'binary');
						return reply(img).type("image/png");
					}
					generateSignature(assets, opts).then( buffer => {
						reply(buffer).type("image/png");
						// Cache for 6 hours.
						redisClient.set(`paper:signature:${digest}`, buffer.toString('binary'), 'EX', 21600);
					});
				});
			});
		},
	});
});

function loadConfig() {
	return new Promise( (resolve, reject) => {
		fs.access("config.json", fs.constants.R_OK | fs.constants.W_OK, err => {
			if (!err) {
				fs.readFile("config.json", (err, data) => {
					try {
						let cfg = JSON.parse(data);
						config = Object.assign(config, cfg);
						resolve();
					} catch (e) {
						reject(e);
					}
				});
			} else {
				console.log('No config.json found! Using defaults!');
				resolve();
			}
		});
	});
}
function generateSignature(assets,opts) {
	return new Promise( (resolve,reject) => {
		new Jimp(opts.size.width, opts.size.height, (err, base) => {
			// Place the background in
			loadBackground(opts.background).then( bg => {
				let backgroundImage;
				if (!bg) {
					backgroundImage = assets.images.background.image;
					if (opts.gradient) {
						backgroundImage = assets.images.background_gradient.image;
					}
				} else {
					bg.cover(opts.size.width, opts.size.height);
					backgroundImage = bg;
				}
				
				base.composite(backgroundImage, 0, 0);
				loadAvatar(opts.profile, opts.size.height).then( avatar => {
					// Username
					new Jimp(opts.size.width, opts.size.height, (err, nameOverlay) => {
						
						// Draw username
						nameOverlay.print(assets.fonts.username.font, 147, 23, opts.profile.name);
						
						// Colourise this layer due to the username
						nameOverlay.color([
							{ apply: 'red', params: [ opts.colour.accent.r ] },
							{ apply: 'green', params: [ opts.colour.accent.g ] },
							{ apply: 'blue', params: [ opts.colour.accent.b ] },
						]);
						// Draw the line down the left
						let hex = Jimp.rgbaToInt(opts.colour.bar.r, opts.colour.bar.g, opts.colour.bar.b, 255);
						if (opts.bar > 0) drawRect(nameOverlay, 0, 0, opts.bar, 100, hex);
						
						new Jimp(opts.size.width, opts.size.height, (err, tagOverlay) => {
						
							// Plop the tag line on
							tagOverlay.print(assets.fonts.open_sans.font, 142, 60, opts.tagline);
							
							tagOverlay.color([
								{ apply: 'red', params: [ opts.colour.tag.r ] },
								{ apply: 'green', params: [ opts.colour.tag.g ] },
								{ apply: 'blue', params: [ opts.colour.tag.b ] },
							]);
							
							base.composite(nameOverlay, 0, 0);
							base.composite(tagOverlay, 0, 0);
							if (opts.logo || !avatar) {
								base.composite(assets.images.paper_logo.image, ( opts.bar==true ? 5 : 0 ), 0);
							} else {
								base.composite(avatar, opts.bar, 0);
							}
							
							base.getBuffer(Jimp.MIME_PNG, (err, buffer) => {
								if (err) throw err;
								console.log("Generated Image");
								resolve(buffer);
							});
						});
						
					});
				});
				
			});
			
		});
	});
}


// Supporting functions
function loadFonts(fonts) {
	let promises = [];
	for (let font of fonts) {
		let pr = Jimp.loadFont(font.source).then( fnt => {
			font.font = fnt;
			return font;
		});
		promises.push(pr);
	}
	return Promise.all(promises).then( fonts => {
		let ret = {};
		for (let font of fonts) {
			let id = font.id;
			delete font.id;
			ret[id] = font;
		}
		return ret;
	});
}
function loadImages(images) {
	let promises = [];
	for (let img of images) {
		let pr = Jimp.read(img.source).then( jimg => {
			img.image = jimg;
			return img;
		});
		promises.push(pr);
	}
	return Promise.all(promises).then( images => {
		let ret = {};
		for (let image of images) {
			let id = image.id;
			delete image.id;
			ret[id] = image;
		}
		return ret;
	});
}
function loadBackground(url) {
	if (!url) {
		return Promise.resolve(null);
	}
	return Jimp.read(url);
}
function loadAvatar(profile, size) {
	
	// Until Jimp Nearest Neighbour is fixed.
	return Jimp.read(`http://cravatar.eu/helmavatar/${profile.id}/${size}.png`);
	
	// Load and crop the avatar where possible.
	// Uses provided skin.
	return new Promise( (resolve, reject) => {
		if (profile.properties) {
			if (profile.properties.length <= 0) return resolve(null);
			
			let texture;
			for (let prop of profile.properties) {
				if (prop.name.toLowerCase() == "textures") {
					texture = prop;
					break;
				}
			}
			
			// No texture(?)
			if (!texture) return resolve(null);
			
			let textureContent = Buffer.from(texture.value, 'base64');
			try {
				textureContent = JSON.parse(textureContent);
			} catch (e) {
				return resolve(null);
			}
			
			// Do he got the textures?
			if (!textureContent.textures) return resolve(null);
			if (!textureContent.textures.SKIN) return resolve(null);
			
			// He doooo!
			Jimp.read(textureContent.textures.SKIN.url)
			.then( skin => {
				
				// Create a new image to put the skin on
				new Jimp(8, 8, (err, avatar) => {
					
					let oldSkins = [
						"069a79f444e94726a5befca90e38aaf5", // Notch
					];
					
					if (oldSkins.indexOf(profile.id) >= 0) {
						// Remove the transparent colour from the helm
						let transparent = skin.getPixelColor(40, 0);
						for (let py=0; py<16; py++) {
							for (let px=40; px<skin.bitmap.width; px++) {
								if (skin.getPixelColor(px, py) === transparent) {
									skin.setPixelColor(4294967040, px, py);
								}
							}
						}
					}
					
					// Copy face on.
					let face = skin.clone();
					face.crop(8,8,8,8);
					avatar.composite(face, 0, 0);
					
					// Copy Face overlay (if any)
					if (skin.bitmap.width >= 64) {
						let helm = skin.clone();
						helm.crop(40,8,8,8);
						avatar.composite(helm, 0, 0);
					}
					
					// Scale her up!
					avatar.resize(size, size, Jimp.RESIZE_NEAREST_NEIGHBOR);
					
					return resolve(avatar);
					
				});
			})
			.catch( e => {
				console.log(e);
				return resolve(null);
			});
			
		} else {
			resolve(null);
		}
	});
}
function loadAssets(assets) {
	
	let pr1 = loadFonts(assets.fonts).then( fonts => {
		return fonts;
	});
	let pr2 = loadImages(assets.images).then( images => {
		return images;
	});
	
	return Promise.all([pr1, pr2]).then( mixed => {
		return { fonts: mixed[0], images: mixed[1]};
	});
}
function drawRect(image, x, y, width, height, hex) {
	for (let px=x; px<width; px++) {
		for (let py=y; py<height; py++) {
			image.setPixelColor(hex, px, py);
		}
	}
}
// Obtains the user profile for the provided username or uuid.
function getUser(user) {
	
	// Dud user.
	if (user.toLowerCase() == "paper-mc") return Promise.resolve({name:"PaperMC", id: "00000000000000000000000000000000"});
	
	if (user.length > 16) {
		// UUID
		return getProfile(user);
	} else {
		return getUUID(user).then(getProfile);
	}
	
}
function getUUID(username) {
	return new Promise( (resolve, reject) => {
		redisClient.get(`mojang:username:${username.toLowerCase()}`, function(err, reply) {
			if (err) return reject(err);
			if (reply != null) {
				console.log("Loading cached response", reply);
				return resolve(reply);
			}
			request({
				url: "https://api.mojang.com/profiles/minecraft",
				method: "POST",
				json: true,
				body: [username]
			}).then( body => {
				if (body.length > 0) {
					redisClient.set(`mojang:username:${username.toLowerCase()}`, body[0].id, 'EX', 300);
					resolve(body[0].id);
				} else {
					resolve(null);
				}
			}).catch( e => {
				resolve(null);
			});
		});
	});
}
function getProfile(uuid) {
	return new Promise( (resolve, reject) => {
		if (!uuid) resolve(null);
		redisClient.get(`mojang:uuid:${uuid.toLowerCase()}`, function(err, reply) {
			if (err)return reject(err);
			if (reply != null) {
				console.log("Loading cached response", reply);
				return resolve(JSON.parse(reply));
			}
			request({
				url: `https://sessionserver.mojang.com/session/minecraft/profile/${uuid}`,
				json: true
			})
			.then( body => {
				if (body) {
					redisClient.set(`mojang:uuid:${uuid.toLowerCase()}`, JSON.stringify(body), 'EX', 300);
					resolve(body);
				} else {
					resolve(null);
				}
			}).catch( e => {
				resolve(null);
			});
		});
		
	});
}